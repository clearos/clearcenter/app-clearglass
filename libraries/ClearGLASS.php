<?php

/**
 * ClearGLASS class.
 *
 * @category   apps
 * @package    clearglass
 * @subpackage libraries
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass/
 */

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\clearglass;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('clearglass');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Stats as Stats;
use \clearos\apps\certificate_manager\Certificate_Manager as Certificate_Manager;
use \clearos\apps\docker\Project as Project;
use \clearos\apps\network\Network_Utils as Network_Utils;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('base/Stats');
clearos_load_library('certificate_manager/Certificate_Manager');
clearos_load_library('docker/Project');
clearos_load_library('network/Network_Utils');

// Exceptions
//-----------

use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearGLASS class.
 *
 * @category   apps
 * @package    clearglass
 * @subpackage libraries
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass/
 */

class ClearGLASS extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const APP_BASENAME = 'clearglass';
    const APP_CERT_DETAIL = 'ClearGLASS engine'; // Please do not translate
    const FILE_NGINX_CERTS = '/var/lib/clearglass/config/ssl/nginx_certs.conf';
    const FILE_SETTINGS = '/var/lib/clearglass/settings/settings.py';
    const FILE_COMPOSE = '/var/lib/clearglass/docker-compose.yml';
    const PATH_CERTS = '/var/lib/clearglass/config/ssl/certs';
    const CONSTANT_PORT = ':9443';  // use blank for 443
    const REQUIRED_MEMORY = 7000000;

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * ClearGLASS constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Returns configured digital certificate.
     *
     * @return string basename of digital certificate
     * @throws Engine_Exception
     */

    public function get_digital_certificate()
    {
        clearos_profile(__METHOD__, __LINE__);

        $certificate_manager = new Certificate_Manager();
        $registered = $certificate_manager->get_registered_certificate(self::APP_BASENAME, self::APP_CERT_DETAIL);

        return $registered;
    }

    /**
     * Returns list of available certificates.
     *
     * @return array list of available certificates
     * @throws Engine_Exception
     */

    public function get_digital_certificates()
    {
        clearos_profile(__METHOD__, __LINE__);

        $certificate_manager = new Certificate_Manager();
        $hostnames = $certificate_manager->get_secure_hostnames();

        return $hostnames;
    }

    /**
     * Returns web interface URL.
     *
     * @return string web interface URL
     * @throws Engine_Exception
     */

    public function get_secure_hostname()
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File(self::FILE_SETTINGS);

        if (!$file->exists())
            return '';

        $hostname = $file->lookup_value('/^CORE_URI\s*=\s*/');

        $hostname = preg_replace('/"/', '', $hostname);
        $hostname = preg_replace('/http[s]:\/\//', '', $hostname);
        $hostname = preg_replace('/:.*/', '', $hostname);

        return $hostname;
    }

    /**
     * Returns secure hostname.
     *
     * @param string $remote_ip IP address of remote client
     * @param string $server_name name of the server host
     *
     * @return string secure hostname
     * @throws Engine_Exception
     */

    public function get_url($remote_ip = '', $server_name = '')
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File(self::FILE_SETTINGS);
        if (!$file->exists())
            return '';

        $url = $file->lookup_value('/^CORE_URI\s*=\s*/');
        $url = preg_replace('/"/', '', $url);

        if (!empty($remote_ip) && !empty($server_name) && Network_Utils::is_private_ip($remote_ip))
            $url = preg_replace('/:\/\/[^:]*/', '://' . $server_name, $url);

        return $url;
    }

    /**
     * Returns state of capability.
     *
     * @return boolean TRUE if system is capable of running app
     * @throws Engine_Exception
     */

    public function is_capable()
    {
        clearos_profile(__METHOD__, __LINE__);

        $stats = new Stats();
        $memory = $stats->get_memory_stats();

        if ($memory['total'] < self::REQUIRED_MEMORY)
            return FALSE;
        else
            return TRUE;
    }

    /**
     * Returns state of initialization.
     *
     * @return boolean TRUE if initialized
     * @throws Engine_Exception
     */

    public function is_initialized()
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File(self::FILE_NGINX_CERTS);

        if (!$file->exists())
            return FALSE;

        $contents = $file->get_contents_as_array();

        foreach ($contents as $line) {
            if (preg_match('/^ssl_certificate/', $line))
                return TRUE;
        }

        return FALSE;
    }

    /**
     * Resets NGINX.
     *
     * @throws Engine_Exception
     */

    public function reset_nginx()
    {
        clearos_profile(__METHOD__, __LINE__);

        Project::restart_container('nginx', self::FILE_COMPOSE);
    }

    /**
     * Sets the digital certificate.
     *
     * @return void
     * @throws Engine_Exception
     */

    public function set_certificate($certificate)
    {
        clearos_profile(__METHOD__, __LINE__);

        Validation_Exception::is_valid($this->validate_certificate($certificate));

        // Register certificate with Certificate Manager
        //----------------------------------------------

        $cert_object[self::APP_CERT_DETAIL] = $certificate;
        $certificate_manager = new Certificate_Manager();
        $certificate_manager->register($cert_object, self::APP_BASENAME, lang('clearglass_app_name'), self::APP_CERT_DETAIL);

        // Update NGINX configlet
        //-----------------------

        $details = $certificate_manager->get_certificate($certificate);

        // Cert: add chain file for NGINX
        if (!empty($details['intermediate-filename'])) {
            $file = new File($details['intermediate-filename'], TRUE);
            $intermediate = $file->get_contents();
        }

        $file = new File($details['certificate-filename']);
        $file->copy_to(self::PATH_CERTS . '/cert.pem', FALSE);

        if (!empty($intermediate)) {
            $file = new File(self::PATH_CERTS . '/cert.pem', TRUE);
            $file->add_lines($intermediate);
        }

        // Key
        $file = new File($details['key-filename']);
        $file->copy_to(self::PATH_CERTS . '/privkey.pem', FALSE);

        $file = new File(self::PATH_CERTS . '/privkey.pem');
        $file->chmod('0640');

        // Configlet
        $file = new File(self::FILE_NGINX_CERTS);
        if ($file->exists())
            $file->delete();

        $file->create('root', 'root', '0644');

        $lines = "ssl_certificate ssl/certs/cert.pem;\n";
        $lines .= "ssl_certificate_key ssl/certs/privkey.pem;\n";
        $file->add_lines($lines);
    }

    /**
     * Sets secure hostname.
     *
     * @param string $hostname secure hostname
     *
     * @return void
     */

    public function set_secure_hostname($hostname)
    {
        clearos_profile(__METHOD__, __LINE__);

        $file = new File(self::FILE_SETTINGS);
        $file->replace_lines('/^CORE_URI\s*=/', 'CORE_URI = "https://' . $hostname . self::CONSTANT_PORT . '"' . "\n");
    }

    ///////////////////////////////////////////////////////////////////////////////
    // V A L I D A T I O N  M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Validation routine for digital certificates.
     *
     * @param string $certificate basename of digital certificate
     *
     * @return string error message if certificate is invalid
     */

    public function validate_certificate($certificate)
    {
        clearos_profile(__METHOD__, __LINE__);

        $certificate_manager = new Certificate_Manager();
        $certificates = $certificate_manager->get_certificates();

        if (!array_key_exists($certificate, $certificates))
            return lang('certificate_manager_certificate_invalid');
    }
}
