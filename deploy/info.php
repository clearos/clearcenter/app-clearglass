<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'clearglass';
$app['version'] = '2.5.27';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('clearglass_app_description');
$app['powered_by'] = array(
    'vendor' => array(
        'name' => 'ClearGLASS',
        'url' => 'http://clear.glass/',
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('clearglass_app_name');
$app['category'] = lang('base_category_server');
$app['subcategory'] = lang('base_subcategory_management');
$app['menu_enabled'] = FALSE;

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-network',
    'app-docker'
);

$app['core_requires'] = array(
    'app-base-core >= 1:2.4.20',
    'app-docker-core',
    'app-events-core',
    'app-mail-routing-core',
    'app-network-core',
    'clearglass-engine',
    '/usr/lib/systemd/system/clearglass.service'
);

$app['core_file_manifest'] = array(
    'network-configuration-event'=> array(
        'target' => '/var/clearos/events/network_configuration/clearglass',
        'mode' => '0755'
    ),
    'onboot-event'=> array(
        'target' => '/var/clearos/events/onboot/clearglass',
        'mode' => '0755'
    ),
    'clearglass.letsencrypt'=> array(
        'target' => '/var/clearos/events/lets_encrypt/clearglass',
        'mode' => '0755'
    ),
);

$app['core_directory_manifest'] = array(
    '/var/clearos/clearglass' => array(),
    '/var/clearos/clearglass/backup' => array(),
);

$app['delete_dependency'] = array(
    'app-clearglass-core',
);
