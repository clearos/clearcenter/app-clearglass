<?php

/**
 * ClearGLASS settings controller.
 *
 * @category   apps
 * @package    clearglass
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearglass/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearGLASS settings controller.
 *
 * @category   apps
 * @package    clearglass
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearglass/
 */

class Settings_Controller extends ClearOS_Controller
{
    protected $app_name = NULL;

    /**
     * ClearGLASS settings constructor.
     *
     * @param string $app_name app that manages the docker app
     *
     * @return view
     */

    function __construct($app_name)
    {
        $this->app_name = $app_name;
    }

    /**
     * ClearGLASS settings controller
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->load->library('docker/Project', $this->project_name);
        $this->lang->load('docker');

        // Load data
        //----------

        try {
            $is_installed = $this->project->is_installed();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        $this->_common('view');
    }

    /**
     * Edit view.
     *
     * @return view
     */

    function edit($init = FALSE)
    {
        $this->_common('edit', $init);
    }

    /**
     * Edit view.
     *
     * @return view
     */

    function limited()
    {
        // Load dependencies
        //------------------

        $this->lang->load('clearglass');

        // Handle form submit
        //-------------------

        $this->page->view_form('clearglass/limited', $data, lang('base_settings'));
    }

    /**
     * View view.
     *
     * @return view
     */

    function view()
    {
        $this->_common('view');
    }

    /**
     * Common view/edit handler.
     *
     * @param string $form_type form type
     *
     * @return view
     */

    function _common($form_type, $init = FALSE)
    {
        // Load dependencies
        //------------------

        $this->load->library('clearglass/ClearGLASS');
        $this->lang->load('clearglass');
        $this->lang->load('base');

        // Handle form submit
        //-------------------

        if ($this->input->post('submit')) {
            try {
                $cert_and_hostname = preg_split('/\|/', $this->input->post('hostname'));

                $this->clearglass->set_certificate($cert_and_hostname[0]);
                $this->clearglass->set_secure_hostname($cert_and_hostname[1]);
                $this->clearglass->reset_nginx();

                $this->page->set_status_updated();
                redirect('/' . $this->app_name . '/settings');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load view data
        //---------------

        $data['form_type'] = $form_type;
        $data['init'] = $init;
        $data['app_name'] = $this->app_name;

        try {
            $certificate = $this->clearglass->get_digital_certificate();
            $certificates = $this->clearglass->get_digital_certificates();

            $secure_hostname = $this->clearglass->get_secure_hostname();
            $data['hostname'] = $certificate . '|' . $secure_hostname;

            foreach ($certificates as $basename => $details) {
                $list = [];
                foreach ($details['hostnames'] as $hostname)
                    $list[$basename . '|' . $hostname] = $hostname;
                $data['hostnames'][$details['name']] = $list;
            }

            // Theme TODO: the group options doesn't like view-mode.  Hack it in here for now.
            if ($form_type != 'edit') {
                $data['hostnames']['view_mode'] = $secure_hostname;
                $data['hostname'] = 'view_mode';
            }
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('clearglass/settings', $data, lang('base_settings'));
    }
}
